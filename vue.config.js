/*
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-20 11:18:00
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-20 16:07:00
 */
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    open: true,
    host: 'localhost',
    port: '8080',
    proxy: {
      [process.env.VUE_APP_BASE_URL]: {
        target: process.env.VUE_APP_SERVICE_URL,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_URL]: '',
        },
      },
    },
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    resolve: {
      alias: {
        '@': resolve('src'),
      },
    },
  },
}
