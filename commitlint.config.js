/*
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-20 09:15:32
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-22 19:47:17
 */
module.exports = {
  extends: ['cz'],
  rules: {
    'type-empty': [2, 'never'],
    'subject-empty': [2, 'never'],
  },
}
