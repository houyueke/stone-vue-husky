/*
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-22 09:22:50
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-22 19:36:31
 */
module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {},
}
