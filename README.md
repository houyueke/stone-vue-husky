<!--
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-19 11:38:11
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-23 09:59:47
-->

# stone-vue

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# 构建代码预提交检查工具链

- `Husky` 拦截git hooks，防止错误的git commit，git push 等
- `ESLint` 查找并修复 JavaScript、Vue代码中的问题
- `Prettier` 代码格式化程序，支持多种语言
- `lint-staged ` 取得所有被提交的文件依次执行写好的任务

### Eslint

- 安装命令

``` javascript
npm install eslint babel-eslint --save-dev
```

- 项目根目录下创建/编辑`.eslintrc.js`文件

```javascript
module.exports = {
  root: true,
  env: {
    node: true,
    es6 : true,
    browser : true
  },
  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
  },
}
```

### Prettier安装

- 安装命令

```javascript
npm install prettier eslint-plugin-prettier eslint-config-prettier --save
```

- 项目根目录下创建`.prettierrc.js`

```javascript
module.exports = {
  semi: false,
  singleQuote: true,
  trailingComma: 'none',
}
```

在该文件可以配置代码美化格式  https://prettier.io/docs/en/options.html

### husky安装过程

- 环境要求

  安装前先检查本地git版本，windows git 版本要求>2.13

- 项目下载安装`husky`

  ```javascript
  npm insall husky@4.2.3 --save-dev
  ```

  `husky` 在安装过程中会在 `.git/hooks` 文件夹中生成一系列的 `git hook` 脚本，命令行提示如下安装信息则安装成功

<img src="C:\Users\szyjy-03\AppData\Roaming\Typora\typora-user-images\image-20210422104831397.png" alt="image-20210422104831397"  />

- `package.json`添加`husky`配置

  ```json
    "husky": {
      "hooks": {
        "pre-commit": "echo \"git commit trigger husky pre-commit hook already\" "
      }
    },
  ```
  `husky pre-commit` 测试

  ```git
  git add .
  git commit -m "test husky pre-commit"
  ```

  这样在进行gti commit 的时候就会看到pre-commit 已经执行了。

  ![image-20210422110657887](C:\Users\szyjy-03\AppData\Roaming\Typora\typora-user-images\image-20210422110657887.png)

### lint-staged安装

- 安装命令

```javascript
npm install lint-staged --save-dev
```

- package.json添加`lint-staged`配置并修改`husky`配置

```json
"husky": {
  "hooks": {
    "pre-commit": "lint-staged"
  }
},
"lint-staged": {
  "*.{js,jsx,vue}": [
    "prettier --write ./src",
    "eslint --ext .vue,.js,.jsx --fix ./src",
    "git add"
  ]
}
```

`husky` 会在 `git commit`的时候，调用 `pre-commit` 钩子，执行 lint-staged，如果代码不符合 `prettier` 配置的规则，会进行格式化；然后执行 `eslint` 的规则进行检查，如果有不符合规则且无法自动修复的，就会停止此次提交。

# 构建git commit 规范化提交

- `Commitizen` 提交规范消息格式提示，支持自定义
- `cz-customizable` 可自定义的Commitizen插件,可帮助实现一致的提交消息

### Commitizen&&cz-customizable

- 安装`Commitizen cli`工具

```javascript
npm install commitizen --save-dev
```

- 安装`Adapter`

官网提供了以下命令用来初始化`Adapter`,但这个适配器使用的是`Angular`的`commit message`。

```javascript
npx commitizen init cz-conventional-changelog --save-dev --save-exact
```

- `cz-customizable`安装

因为`cz-customizable`支持一定程度上的自定义，可以根据团队不同作出适当调整，所以我们选用该适配器

```javascript
npm install cz-customizable --save-dev
```

- `package.json`增加配置

```json
{
  "scripts": {
    "commit": "git-cz",
      ...
  },
  "config": {
    "commitizen": {
      "path": "node_modules/cz-customizable"
    }
  }
}
```

此时我们可以执行`npm run commit` 脚本。

- 在项目根目录下创建`.cz-config.js`

根据`node_modules/cz-customizable/cz-config-EXAMPLE.js`配置`git cz`时弹出的message和对应的输入或者选项,详细配置如下：

```javascript
/*
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-20 09:02:36
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-20 09:03:33
 */
module.exports = {
  types: [
    {
      value: 'feat',
      name: '✨  feat:    增加新特性/新功能 (A new feature)',
    },
    {
      value: 'fix',
      name: '🐞  fix:    修复bug (A bug fix)',
    },
    {
      value: 'refactor',
      name:
        '🛠  refactor:    重构 (A code change that neither fixes a bug nor adds a feature)',
    },
    {
      value: 'docs',
      name: '📚  docs:    仅包含文档的修改 (Documentation only changes)',
    },
    {
      value: 'test',
      name:
        '🏁  test:    添加或修改测试代码 (Add missing tests or correcting existing tests)',
    },
    {
      value: 'chore',
      name:
        "🗯  chore:    杂项修改 (Changes that don't modify src or test files. Such as updating build tasks, package manager)",
    },
    {
      value: 'style',
      name:
        '💅  style:    格式化修改 (Code Style, Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc))',
    },
    {
      value: 'revert',
      name: '⏪  revert:    撤销某次提交 (Revert to a commit)',
    },
    {
      value: 'perf',
      name:
        '📈  perf:    提高性能的修改 (A code change that improves performance)',
    },
  ],
  messages: {
    type: "Select the type of change that you're committing:",
    scope: '\nDenote the SCOPE of this change (optional):',
    // used if allowCustomScopes is true
    customScope: 'Denote the SCOPE of this change:',
    subject: 'Write a SHORT, IMPERATIVE tense description of the change:\n',
    body:
      'Provide a LONGER description of the change (optional). Use "|" to break new line:\n',
    breaking: 'List any BREAKING CHANGES (optional):\n',
    footer:
      'List any ISSUES CLOSED by this change (optional). E.g.: #31, #34:\n',
    confirmCommit: 'Are you sure you want to proceed with the commit above?',
  },

  // allowCustomScopes: true,
  allowBreakingChanges: ['feat', 'fix'],
  // skip any questions you want
  skipQuestions: ['scope', 'customScope', 'breaking', 'footer', 'body'],

  // limit subject length
  // subjectLimit: 100,
  scopes: [],
  // allowCustomScopes: true,
  // allowBreakingChanges: ['feat', 'fix'],
}

```

### @commitlint/cli && commitlint-config-cz

- 安装@commitlint/cli

```javascript
npm install @commitlint/cli --save-dev
```

- `husky`添加`commit-msg `  hooks

```json
{
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged",
      "commit-msg":"commitlint -E HUSKY_GIT_PARAMS"
    }
  }
}
```

- 安装 `commitlint-config-cz`

`commitlint-config-cz` 合并 `cz-customizable` 的配置 {types,scopes,scopeOverrides} 和 `commitlint` 的配置 {type-enum,subject-enum,...}

```javascript
npm install commitlint-config-cz --save-dev
```

- 在项目根目录下创建`commitlint.config.js`,并写入以下内容

```javascript
module.exports = {
  extends: ['cz'],
  rules: {
    "type-empty": [2, never],
    "subject-empty": [2, never]
  },
}
```


