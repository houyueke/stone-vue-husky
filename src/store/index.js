/*
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-19 11:37:38
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-19 17:10:19
 */
import Vue from 'vue'
import Vuex from 'vuex'
import Login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},

  actions: {},
  modules: {
    Login,
  },
})
