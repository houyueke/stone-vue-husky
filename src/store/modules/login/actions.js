import { SET_TOKEN } from './mutationsTypes.js'

export default {
  set_token({ commit }, token) {
    commit(SET_TOKEN, token)
  },
}
