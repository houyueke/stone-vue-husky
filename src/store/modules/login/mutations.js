import { SET_TOKEN } from './mutationsTypes.js'

export default {
  [SET_TOKEN](state, token) {
    state.token = token
  },
}
