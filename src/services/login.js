/*
 * @Descripttion: 用户登录接口
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-20 10:58:17
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-20 15:53:39
 */
import { request } from '../plugins/axios'

export function login(data) {
  return request({
    url: '/v1/login',
    method: 'post',
    data,
  })
}

export function info(params) {
  return request({
    url: '/v1/info',
    method: 'get',
    params,
  })
}

export function test(params) {
  return request({
    url: '/',
    method: 'get',
    params,
  })
}

/**
 * 下载文件
 * @param {*} params: 请求参数
 */
export function downFileAction(params) {
  return request({
    url: '/download',
    method: 'get',
    params,
    responseType: 'blob',
  })
}
