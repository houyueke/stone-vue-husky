/*
 * @Descripttion:
 * @version:
 * @Author: houyueke
 * @Date: 2021-04-19 11:37:38
 * @LastEditors: houyueke
 * @LastEditTime: 2021-04-20 15:01:24
 */
import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import 'normalize.css/normalize.css'
import './plugins/element.js'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
