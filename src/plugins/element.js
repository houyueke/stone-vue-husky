import Vue from 'vue'
import {
  Button,
  Select,
  Pagination,
  Dialog,
  Input,
  Table,
  TableColumn,
  Form,
  FormItem,
  MessageBox,
  Notification,
  Message,
  Loading,
} from 'element-ui'

Vue.use(Button)
Vue.use(Select)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Input)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Select)
Vue.use(Loading.directive)

Vue.prototype.$ELEMENT = {
  size: 'small',
  zIndex: 3000,
}

Vue.prototype.$loading = Loading.service
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$notify = Notification
Vue.prototype.$message = Message
