import axios from 'axios'
import qs from 'qs'

// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const config = {
  baseURL: process.env.VUE_APP_BASE_URL,
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
}

const _axios = axios.create(config)

_axios.interceptors.request.use(
  (config) => {
    config.method === 'post'
      ? (config.data = qs.stringify({ ...config.data }))
      : (config.params = {
          ...config.params,
          _t: Date.parse(new Date()) / 1000,
        })
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

_axios.interceptors.response.use(
  (response) => {
    if (response.status === 200) {
      return response.data
    }
  },
  (error) => {
    return Promise.reject(error)
  }
)

export { _axios as request }
